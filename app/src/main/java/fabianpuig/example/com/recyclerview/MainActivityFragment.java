package fabianpuig.example.com.recyclerview;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * A placeholder fragment containing a simple view.
 */
public class MainActivityFragment extends Fragment {

    private RecyclerView recyclerView;
    CartaAdapter adapter;

    private JSONArray cartas;
    private String url = "https://api.magicthegathering.io/v1/cards?colors=red,white,blue";

    public MainActivityFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main, container, false);

        recyclerView = view.findViewById( R.id.recycler_view );
        // inicializamos el JSONArray
        cartas = new JSONArray();
        // creamos un adapter
        adapter = new CartaAdapter( getActivity(), cartas );


        // indicamos el layout manager que gestionara la distribución de los datos en pantalla
        // ( comentar/ descomentar a gusto para probar cada uno)
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(), 2);
        recyclerView.setLayoutManager( gridLayoutManager );

        //LinearLayoutManager linearLayoutManager = new LinearLayoutManager( getContext() );
        //recyclerView.setLayoutManager( linearLayoutManager );

        // orientation: 0 horizontal / 1 vertical
        //StaggeredGridLayoutManager staggeredGridLayoutManager = new StaggeredGridLayoutManager( 2, 1 );
        //recyclerView.setLayoutManager( staggeredGridLayoutManager );

        // Añadimos una decoracion para añadir espacio entre elementos
        recyclerView.addItemDecoration( new SpacesItemDecoration( 10 ) );

        recyclerView.setAdapter( adapter );

        adapter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.i("test", "setOnClickListener position; " + recyclerView.getChildAdapterPosition( view ) );
            }
        });


        return view;
    }

    @Override
    public void onStart() {
        super.onStart();

        new AsyncTaskCartas(url, new AsyncTaskCartas.AsyncResponse() {
            @Override
            public void processFinish(String res) {

                if( res != null ){
                    Log.i("test", res );

                    try {
                        JSONObject object = new JSONObject( res );
                        cartas = object.getJSONArray( "cards" );
                        // indicamos al adapter que actualize la lista de cartas
                        adapter.swap( cartas );

                        for (int i = 0; i < cartas.length(); i++) {
                            Log.i("cartas", "Posicion " + i + ": " + cartas.get( i ).toString() ) ;
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).execute();
    }
}
