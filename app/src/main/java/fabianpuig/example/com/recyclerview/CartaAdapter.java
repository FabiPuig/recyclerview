package fabianpuig.example.com.recyclerview;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Fabian on 04/03/2018.
 */

public class CartaAdapter extends RecyclerView.Adapter<CartaAdapter.ViewHolder> implements View.OnClickListener{

    private Activity activity;
    private JSONArray jsonArray;
    private View.OnClickListener listener;

    /** Constructor
     *
     */
    public CartaAdapter(Activity activity, JSONArray jsonArray) {

        this.activity = activity;
        this.jsonArray = jsonArray;
    }

    public void setOnClickListener(View.OnClickListener onClickListener) {
        this.listener = onClickListener;
    }

    @Override
    public void onClick(View view) {
        if( listener != null ){
            listener.onClick(view);
        }
    }

    /**Encargado de crear los ViewHolder necesarios para los elementos de la colección
     *
     */
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // creamos una vista
        View view = LayoutInflater.from( parent.getContext()).inflate(R.layout.cell, null);
        // añadimos el listener a la vista
        view.setOnClickListener(this);
        // devolvemos un viewholder que guardara las referencias de la vista
        return new ViewHolder(view);
    }

    /** Actualiza los datos de un ViewHolder existente
     *
     */
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        try {
            // el viewholder mostrara la informacion mediante el metodo indicado
            holder.onBind( jsonArray.getJSONObject( position ) );
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /** Elementos de la colección de datos
     *
     */
    @Override
    public int getItemCount() {
        if( jsonArray != null ){
            return jsonArray.length();
        }
        else{
            return 0;
        }
    }

    /** Usaremos este metodo para actualizar la informacion en el adapter
     *
     * @param jsonArray
     */
    public void swap( JSONArray jsonArray ){
        this.jsonArray = jsonArray;
        notifyDataSetChanged();
    }


    /** Esta clase nos va a guardar las referencias (findViewById()) de cada una de las celdas/lineas
     *
     */
    public class ViewHolder extends RecyclerView.ViewHolder{

        private ImageView imageView;
        private TextView textView;

        /** Constructor: referenciamos
         *
         * @param itemView
         */
        public ViewHolder(View itemView) {
            super(itemView);

            imageView = itemView.findViewById( R.id.iv_cell );
            textView = itemView.findViewById( R.id.tv_cell );
        }

        /** Usaremos este metodo para mostrar la información en la celda/linea
         *
         */
        public void onBind(JSONObject jsonObject){

            textView.setText( jsonObject.optString( "name" ) );
            GlideApp.with( activity )
                    .load( jsonObject.optString( "imageUrl" ) )
                    .error( R.drawable.ic_error )
                    .into( imageView );

        }

    }
}
